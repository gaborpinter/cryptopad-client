import { Component, OnInit } from '@angular/core';
import { PadService } from 'src/app/services/pad/pad.service';
import { Router, ROUTER_CONFIGURATION } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public consoleMessage: String

  constructor(
    private service: PadService,
    private router: Router
  ) { }

  ngOnInit(): void { }

  createNewCryptopad(): void {
    this.service.createPadAndGetPadPath()
      .subscribe(
        (path: string) => {
          this.router.navigateByUrl(path)
        },
        (error: any) => {
          console.log(error);
          this.consoleMessage = 'Something went wrong! Check console for error messages!';
        }
      )
  }
}
