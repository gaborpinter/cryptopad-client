import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PadSocketService } from '../../services/pad-socket/pad-socket.service';
import * as rxjs from 'rxjs';
import { startWith } from 'rxjs/operators';
import { Pad } from '../../models/Pad';
import { CryptoService } from '../../services/crypto/crypto.service';
import { PadService } from '../../services/pad/pad.service';
import { State } from '../../enums/state';
import { UpdateRequest } from '../../models/UpdateRequest';

@Component({
  selector: 'app-pad',
  templateUrl: './pad.component.html',
  styleUrls: ['./pad.component.scss']
})
export class PadComponent implements OnInit, OnDestroy {

  public pad: Pad; 
  private subscription: rxjs.Subscription;
  public url: string;
  public buttonText: string;
  private padSecret: string;
  private clientSecret: string;
  private state: State;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cryptoService: CryptoService,
    private padSocketService: PadSocketService,
    private padService: PadService
  ) { }

  ngOnInit(): void {
    this.state = State.loading;
    this.url = window.location.href;
    
    const id = this.route.snapshot.params['id'];
    this.padSecret = this.router.parseUrl(this.router.url).queryParams['secret'] || '';
    this.clientSecret = this.cryptoService.createSecret();

    this.padService.getPadDecrypted(id, this.padSecret)
      .subscribe((pad: Pad) => {
        this.padSocketService.joinPad(id, this.clientSecret);
        this.subscribeToChangesInPad(id);
        this.state = State.done;
      }, (error: any) => {
        this.state = State.error;
      });
  }

  private subscribeToChangesInPad(id: string) {

    const starter = { hmac: null, pad: { id: id, content: 'Loading...'} };

    this.subscription = this.padSocketService.updateRequest
      .pipe(startWith(starter))
      .subscribe((updateRequest: UpdateRequest) => {
        if (updateRequest.pad.content == 'Loading...') {
          this.pad = {
            id: updateRequest.pad.id,
            content: updateRequest.pad.content
          }
        } else {       
          const computedHash = this.cryptoService.hash(updateRequest.pad.content, this.clientSecret);
          if (computedHash == updateRequest.hmac) {
            this.pad = {
              id: updateRequest.pad.id,
              content: this.cryptoService.decrypt(updateRequest.pad.content, this.padSecret)
            }
          } else {
            this.state = State.error;
          }
        }
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  updateContent() {
    const encryptedContent = this.cryptoService.encrypt(this.pad.content, this.padSecret);
    const pad: Pad = {
      id: this.pad.id,
      content: encryptedContent
    };

    const hmac = this.cryptoService.hash(encryptedContent, this.clientSecret);
    const updateRequest: UpdateRequest = { pad, hmac };
    
    this.padSocketService.updatePad(updateRequest);
  }

  isStateLoading() { return this.state === State.loading; }
  isStateDone() { return this.state === State.done; }
  isStateError() { return this.state === State.error; }
}
