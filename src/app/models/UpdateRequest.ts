import { Pad } from './Pad'

export interface UpdateRequest {
    pad: Pad;
    hmac: string
}