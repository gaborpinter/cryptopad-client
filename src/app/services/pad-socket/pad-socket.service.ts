import { Injectable } from '@angular/core';
import { Pad } from '../../models/Pad'
import * as Socket  from 'ngx-socket-io';
import * as rxjs from 'rxjs';
import { UpdateRequest } from '../../models/UpdateRequest';

@Injectable({
  providedIn: 'root'
})
export class PadSocketService {

  public updateRequest: rxjs.Observable<UpdateRequest> = this.socket.fromEvent<UpdateRequest>('padUpdateFromServer');

  constructor(private socket: Socket.Socket) {  }

  joinPad(id: string, clientSecret: string): rxjs.Observable<Pad> {
    return this.socket.emit('joinRequestFromClient', id, clientSecret);
  }

  updatePad(updateRequest: UpdateRequest): rxjs.Observable<Pad> {
    return this.socket.emit('padUpdateFromClient', updateRequest);
  }
}
