import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  constructor() { }

  public decrypt(cipherText: string, secret: string): string {
    var decrypted: CryptoJS.DecryptedMessage = CryptoJS.AES.decrypt(cipherText, secret, {
      mode: CryptoJS.mode.CFB,
      padding: CryptoJS.pad.NoPadding
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
  }

  public encrypt(plainText: string, secret: string): string {
    return CryptoJS.AES.encrypt(plainText, secret, {
      mode: CryptoJS.mode.CFB,
      padding: CryptoJS.pad.NoPadding
    }).toString();
  }

  public createSecret(): string {
    return CryptoJS.lib.WordArray.random(128 / 8).toString();
  }

  public hash(plainText: string, secret: string): string {
    return CryptoJS.HmacSHA1(plainText, secret).toString(CryptoJS.enc.Hex);
  }
}
